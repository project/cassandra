<?php 

/**
 * @File
 * provides admin interface for cassandra settings
 */

function cassandra_admin_form() {
  $form = array();
  $form['cassandra_thrift_path'] = array(
        '#type' => 'textfield',
        '#title' => t('Thrfit lib path'),
        '#default_value'=>variable_get("cassandra_thrift_path", CASSANDRA_DEFAULT_THRIFTPATH),
    );
    
  $form['cassandra_default_keyspace'] = array(
    '#type' => 'textfield',
    '#title' => t('Default keyspace'),
    '#default_value'=>variable_get("cassandra_default_keyspace", CASSANDRA_DEFAULT_KEYSPACE ),
  );  
  
  $form['cassandra_default_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Default user'),
    '#default_value'=>variable_get("cassandra_default_user", CASSANDRA_DEFAULT_USER),
  ); 
  
  $form['cassandra_host']=array(
    '#type' => 'textfield',
      '#title' => t('hostname or ip'),
  '#default_value'=>variable_get("cassandra_host", CASSANDRA_DEFAULT_HOST),
  );
  
  $form['cassandra_port']=array(
    '#type' => 'textfield',
    '#title' => t('Port'),
    '#default_value'=>variable_get("cassandra_port", CASSANDRA_DEFAULT_PORT),
  );

  $form['cassandra_read_buffer_size']=array(
    '#type' => 'textfield',
    '#title' => t('Default read buffer size'),
    '#default_value'=>variable_get("cassandra_read_buffer_size", DEFAULT_READ_BUFFER_SIZE),
  );
  
  $form['cassandra_write_buffer_size']=array(
    '#type' => 'textfield',
    '#title' => t('Default write buffer size'),
    '#default_value'=>variable_get("cassandra_write_buffer_size", DEFAULT_WRITE_BUFFER_SIZE),
  );
   return system_settings_form($form);
}