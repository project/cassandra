README
--------------------------------------------------------------------------------
The Cassandra module is based on the cassandra thrift inteface.
http://incubator.apache.org/thrift/

This module is an API that will help you access your Cassandra server to insert new columns, get columns information and do a batch insert. The module can be a good start to create other modules that use Cassandra instead of mysql for performance reasons.


NOTE: This module needs the thrift lib to be compiled and installed on your system.

HOW TO USE:

  1.install the module as usual "copy the module to {Drupal path}/sites/{site name}/modules" then enable it form the admin panel.

  2.go to "Cassandra configurations" in the "Site configuration"
    "Thrfit lib path": insert the path for your compiled thrift lib
    "hostname or ip": the hostname or ip address for your cassandra server
    "Port": the port of cassandra server

  3.now you can start using the functions in this modules
    to insert new column you can use the function
      cassandra_insert($keyspace,$keyuserid,$columnpath,$consistency_level,$value)

    to do a batch insert
      cassandra_batch_insert($keyspace,$keyuserid,$mutation,$consistency_level)

    to get a column from the server
      cassandra_get($keyspace,$keyuserid,$columnparent,$predicate,$consistency_level)

    now you can use these function to create diffrent object types that cassnadra uses
      to create a column object:
        cassandra_column($column_name,$column_value)

      to create a super column object:
        cassandra_supercolumn($supercolumn_name,$columns= array())

      to create a columnpath object:
        cassandra_columnpath($column_family,$super_column,$column)

      to create a columnparent object:
        cassandra_columnparent($column_family,$super_column)

      to create a predicate that holds a slicerange
        cassandra_predicate($start="",$finish="")

      to create a columnorSupercolumn holder 
        cassandra_cosc($supercolumn)

    For more information about using the functions, you can check the cassandra_phonebook sample module which you can find inside the example folder with this package.
    
    
    TODO: 
    Add support for phpcassa library
    Add hook_enable
    Add auto identify thrift location